package com.andrei1058.netherportal.listeners;

import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.utils.PermissionUtil;

import static com.andrei1058.netherportal.NetherPortal.getDatabase;

public class GuildJoin extends ListenerAdapter {

    @Override
    //When the bot joins a guid
    public void onGuildJoin(GuildJoinEvent e) {
        if (!getDatabase().isRankedTableExist(e.getGuild().getId())) {
            getDatabase().createRankedTable(e.getGuild().getId());
        }
    }

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent e) {
        if (getDatabase().isUserRanked(e.getUser().getIdLong(), e.getGuild().getId())) {
            try {
                for (Long rank : getDatabase().getUserRanks(e.getGuild().getId(), e.getUser().getIdLong())) {
                    Role role = e.getGuild().getRoleById(rank);
                    if (role != null) {
                        e.getGuild().getController().addRolesToMember(e.getMember(), role);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("Insufficient perms on " + e.getGuild().getId());
            }
        }
    }
}
